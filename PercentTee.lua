-----------------------------------------------------------------------------------------------
-- Client Lua Script for PercentTee
-- Copyright (c) Commercial Slaxxor @ Jabbit 
-----------------------------------------------------------------------------------------------
-- PercentTee Module Definition
-----------------------------------------------------------------------------------------------

local PercentTee = {} 
 
-----------------------------------------------------------------------------------------------
-- Locals
-----------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------
-- Initialization
-----------------------------------------------------------------------------------------------

function PercentTee:Init()
	local bHasConfigureFunction = false
	local strConfigureButtonText = ""
	local tDependencies = {
		"ChatLog"
	}
    Apollo.RegisterAddon(self, bHasConfigureFunction, strConfigureButtonText, tDependencies)
end
 
function PercentTee:OnLoad()
	local c = Apollo.GetAddon("ChatLog")
	local o = c.OnChatInputReturn

	function c:OnChatInputReturn(a, b, c) -- Hacky but works.
		local T = GameLib.GetTargetUnit()
		return o(self, a, b, string.gsub(c, "%%t", (T and T:GetName() or ((GameLib.GetPlayerUnit():GetGender() == 1 and "her" or "his") .. " imaginary friend"))))
	end
end

-----------------------------------------------------------------------------------------------
-- PercentTee Functions
-----------------------------------------------------------------------------------------------



-- Do the things
PercentTee:Init()
